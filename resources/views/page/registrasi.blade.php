@extends('layout.master')

@section('title')
<h1>Buat Account Baru</h1>
@endsection

@section('content')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
      <label for="fn">First Name :</label>
      <br />
      <br />
      <input type="text" name="fName" id="fn" />
      <br />
      <br />
      <label for="ln">Last Name :</label>
      <br />
      <br />
      <input type="text" name="lName" id="ln" />
      <br />
      <br />
      <label for="gender">Gender</label>
      <br />
      <input type="radio" name="gender" id="gender" /> Male
      <br />
      <input type="radio" name="gender" id="gender" /> Female
      <br />
      <br />
      <label for="Nationality">Nationality</label>
      <br />
      <select name="Nationality" id="Nationality">
        <option value="id">Indonesia</option>
        <option value="us">Amerika</option>
        <option value="uk">Inggris</option>
      </select>
      <br />
      <br />
      <label for="language">Language Spoken</label>
      <br />
      <br />
      <input type="checkbox" name="language" id="language" />Bahasa Indonesia
      <br />
      <input type="checkbox" name="language" id="language" />English
      <br />
      <input type="checkbox" name="language" id="language" />Other
      <br />
      <br />
      <label for="bio">Bio</label>
      <br />
      <br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
      <br />
      <br />
      <input type="submit" name="submit" value="Sign Up" />
    </form>
@endsection
