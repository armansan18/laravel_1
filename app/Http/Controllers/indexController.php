<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
    public function index(){
        return view('page.home');
    }

    public function table(){
        return view('page.data-table');
    }
}
