<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function daftar(){
        return view('page.registrasi');
    }

    public function kirim(Request $request){
        // dd($request->all());
        $namaDepan = $request['fName'];
        $namaBelakang = $request['lName'];

        return view('page.welcome', compact("namaDepan", "namaBelakang"));
    }
}
